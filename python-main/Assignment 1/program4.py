''' program4: Write a program to check whether the number is
    divisible by 5.
input:55
output:55 is divisible by 5 
'''
a = int(input("Enter number:"))

if a % 5 == 0 :
    print(a,"is divisible by 5")
else:
    print(a,"is not divisible by 5")
    

''' output: 55 is by 5 '''
