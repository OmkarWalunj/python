''' program7: Write a program to take an integer ranging from 0 to 6
    and  corresponding weekday.
input:2
output:wednesday
'''
a = int(input("Enter number between range (0,6):"))

if a == 0 :
    print(a,"Monday")

elif a == 1:
    print("Tuesday")
elif a == 2:
    print("Wednesday")
elif a == 3:
    print("Thursday")
elif a == 4:
    print("Friday")
elif a == 5:
    print("Saturday")
elif a == 6:
    print("Sunday")
else:
    print("Invalid Imput")
    

''' output: Wednesday '''
