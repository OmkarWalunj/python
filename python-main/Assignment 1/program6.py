''' program6: Write a program to check whether the character is
    alphabet or not.
input:'v'
output:'v' is an alphabet 
'''
var = input("Enter character:")

if (var >= 'a' and var <= 'z') or (var >= 'A' and var <= 'Z'):
    print(var,"is an alphabet")
else:
    print(var,"is not an alphabet")
    

''' output: 'v' is an aplphabet '''
