''' program9: Write a program to take an integer ranging from 0 to 6
    and  corresponding weekday.
input:2
output:wednesday
'''
x = input("Enter character:")

if x == 'a' :
    print("vowel")
elif x == 'e':
    print("vowel")
elif x == 'i':
    print("vowel")
elif x == 'o':
    print("vowel")
elif x == 'u':
    print("vowel")
else:
    print("COnstant")
    

''' output:>>>Enter Character: 'a'
              vowel
           >>>Enter Character: 'b'
              Constant
'''
