''' program5: Write a program to take an integer ranging from 0 to 6
    and  corresponding weekday.
input:2
output:wednesday
'''
a = int(input("Enter number between range (0,6):"))

if a == 0 :
    print(a,"Monday")

elif a == 1:
    print(a,"Tuesday")
elif a == 2:
    print(a,"Wednesday")
elif a == 3:
    print(a,"Thursday")
elif a == 4:
    print(a,"Friday")
elif a == 5:
    print(a,"Saturday")
elif a == 6:
    print(a,"Sunday")
else:
    print(a,"Invalid Imput")
    

''' output: Wednesday '''
