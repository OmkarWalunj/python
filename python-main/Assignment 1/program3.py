''' program3: Write a program to find  whether the number is
    even or odd.
input:4
output:4 is an even number 
'''
a = int(input("Enter number:"))

if a % 2 == 0 :
    print(a,"is an even number")
else:
    print(a,"is odd number")
    

''' output: 4 is the even number '''
