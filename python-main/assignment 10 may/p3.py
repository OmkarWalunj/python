'''
write a program to print prime number between range.
(a number is divisible by  1 or number itself)

'''
start = int(input("Enter start of range:"))
end = int(input("Enter end of range:"))

for x in range (start ,end+1):
    count=0
    for y in range (2,x):
        if x%y==0:
            count+=1
    if count==0:
        print(x,end=" ")
        
