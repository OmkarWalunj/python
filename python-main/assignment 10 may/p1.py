'''
write a program that print the niven number ranging in 1 to 100
(example :Suppose 18 is a number then 1+8 = 9 and 18%9==0 called niven number
'''
start = int(input("Enter start of range:"))
end = int(input("Enter end of range:"))

for x in range (start ,end+1):
    temp=x
    sum=0
    while x!=0:
        rem=x%10
        sum=sum+rem
        x=x//10
    if temp% sum == 0:
        print(temp ,end=" ")
