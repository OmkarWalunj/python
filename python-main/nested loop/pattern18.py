''' Nested loop   '''
'''write a program of pattern
     A
     2 1
     C B A
     4 3 2 1
'''

rows = int(input ("Enter no of rows :"))

for i in range(rows):
    num = i+65
    for j in range(i+1):
        if i % 2 ==0:
            print(chr(num),end=" ")
        else:
            print(num-64,end =" ")
        num -= 1
        
    print()   

        
