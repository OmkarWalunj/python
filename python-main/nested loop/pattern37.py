'''
     5 4 3 2 1 2 3 4 5
       4 3 2 1 2 3 4
         3 2 1 2 3
           2 1 2
             1
'''

rows = int (input("Enter rows:"))

for x in range (rows):
    num=rows-x
    for y in range (x):
        print(" ",end=" ")
    for z in range((rows-x)*2-1):
        if z<rows-x -1:
            print(num,end=" ")
            num -=1
        else:
            print(num,end=" ")
            num=num+1
    print()
