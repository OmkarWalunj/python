'''
           1
         1 2 1
       1 2 3 2 1
     1 2 3 4 3 2 1
'''

rows = int (input("Enter rows:"))

for x in range (rows):
    num=1
    for y in range (rows-x-1):
        print(" ",end=" ")
    for z in range(2*x +1):
        if z<x:
            print(num,end=" ")
            num +=1
        else:
            print(num,end=" ")
            num=num-1
    print()
