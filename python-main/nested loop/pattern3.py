''' Nested loop   '''
'''write a program of pattern
     1  4  9
     16 25 36
     49 64 81
'''

rows = int(input ("Enter no of rows :"))
cols = int(input ("Enter no of cols :"))

num = 1
for i in range(rows): 
    for j in range(cols):
        print (num*num,end="  ")
        num = num+1
    print()
    
