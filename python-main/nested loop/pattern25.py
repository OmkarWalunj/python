'''
        A
     B  A
   C B  A
 D C B  A
 '''
rows = int (input("Enter Rows:"))

for x in range (rows):
    num = x+65
    for y in range (rows - x -1):
        print(" ",end = " ")
    for z in range (x+1):
        print(chr(num),end = " ")
        num = num-1
    print()
