''' Nested loop   '''
'''write a program of pattern
     1
     2 1
     3 2 1
'''

rows = int(input ("Enter no of rows :"))

for i in range(rows):
    num = i+1
    for j in range(i+1):
        print (num,end=" ")
        num -= 1
    print()   

        
