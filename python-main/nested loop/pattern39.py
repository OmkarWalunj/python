'''
           1 
        2  3  4
     4  5  6  7  8
   9 10 11 12 13 14
'''
rows= int (input("enter rows:"))

num=1
for x in range (rows):
    
    for y in range (rows-x-1):
        print(" ","\t",end=" ")
    for z in range (2*x+1):
        print(num,"\t",end=" ")
        num+=1
    print()
