''' write a program pattern
     A A A A
     B B B
     C C
     D
'''

rows = int (input("Enter no of rows:"))

for x in range (rows):
    num = x+65
    for y in range (rows-x):
        print(chr(num),end=" ")
    print()
