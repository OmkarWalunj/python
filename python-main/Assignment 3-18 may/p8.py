'''
           1
         A A A
       3 3 3 3 3
     B B B B B B B
       3 3 3 3 3
         A A A
           1
'''

rows = int (input("Enter rows:"))


for x in range (rows):
   
    num1=1+x
    for y in range (rows-x-1):
        print(" ",end=" ")
    for z in range(2*x +1):
        num=64+x
        if x%2==0:
            print((num1),end=" ")
        else:
            print(chr(num),end=" ")
    num+=1
    print()
