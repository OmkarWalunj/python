'''
   program 3: Write a program that accept a number from user .
   and print the minimum digit from that number .
   Input:12357798
   output : The minimum digit of that number is 1
'''

a= int (input("Enter Number:"))

min=9
while a!=0:
    rem=a%10
    if (min>rem):
        min=rem
    a=a//10
print("The Minimum digit of that number is",min)
