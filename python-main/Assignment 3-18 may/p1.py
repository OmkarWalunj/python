'''
   program1 :Write a program that Series of palindrome ranging in 300 to 600
   Output : 313,323,333,343....
'''

x= int (input("Enter Starting range:"))
y = int(input("Enter Ending range:"))

for num in  range (x,y+1):
    sum=0
    temp=num

    while (temp!=0):
        reminder = temp%10
        sum= (sum*10)+reminder
        temp=temp//10
    if (num==sum):
        print(num,end=" ")
