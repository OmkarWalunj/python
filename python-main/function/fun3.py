#  Types of parameter 
# 1. positonal paraeter

def fun(x,y):
    print("in fun"))
fun()      # Type error
fun(10)    # Typeerror
fun(10,20) # in fun

# 2. Default parameter

def fun (x,y=20):
    print(x)
    print(y)
fun(10,30)   #10,30
fun(10)      #10,20
fun()        #typeerror

def fun(x=20,y):
    print(x)
    print(y)
fun(10,30)   #syntaxerror
fun(10)      #syntaxerror
fun()        #syntaxerror
