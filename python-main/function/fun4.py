
#  Keyword arguments/parameters

def employeeinfo(name,sal):
    print("employee Name :",name)
    print("EMPLOYEE sal:",sal)
employeeinfo(60.5,"Kanha")
employeeinfo(name="kanha",sal=60.5)


# variable no of arguments

def sum(a,b):
    print("sum:",(a+b))
sum(10,20)
#sum(10,20,30)         #Type error

def sum(a,*b):
    print(type(a))
    print(a)
    print(type(b))
    print(b)
sum(10)
sum(10,20)
sum(10,20,30,40)
'''
def employee(*emp):
    print(emp)
employee(name="kanha",sal=60.5)        #TypeError: employee() got an unexpected keyword argument 'name'
'''
# variable no of keyward arguments

def employee(x,**emp):
    print(emp)
    print(x)
employee(60,sal=60.5)
#employee('name'="kanha",'sal'=60.5)           #SyntaxError: expression cannot contain assignment, perhaps you meant "=="?
employee('name'==20)
