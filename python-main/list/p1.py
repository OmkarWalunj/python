
# To create a list 

l1=list()

#methods
#append
l1.append("omkar")
l1.append("Aniket")
l1.append(5)
l1.append("Tejas")
l1.append(2.5)

#copy
l2=l1.copy()
print(l2)
#clear
l1.clear()
print(l1)

#count
print(l2.count("Omkar"))
print(l2.count("omkar"))

#extend
l2.extend("Ram")
print(l2)

#insert
l2.insert(len(l2),"Sham")
print(l2)

#index
print(l2.index("omkar"))

#pop
l2.pop()
print(l2)

#remove
l2.remove("Om")
print(l2)

#reverse
l2.reverse()
print(l2)

#sort
l2.sort()
print(l2)


