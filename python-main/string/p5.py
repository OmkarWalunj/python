
'''
Program 17. Write a program which accepts two strings from the user
and appends the second string after the first string(Implement strcat()).
Input: FirStr SecStr
Output: FirStrSecStr
'''
str1 = input("Enter string : ")
str2 = input("Enter string : ")

str3=str1+str2
print(str3)

'''
Program 18. Write a program which accepts two strings from the
user and appends N characters of second string after first
string(Implement strncat()).
Input: FirStr SecStr
N : 4
Output: FirStrSecS
'''
str1 = input("Enter string : ")
str2 = input("Enter string : ")

str3=''
N = int(input("Enter number :"))
ch=str2[0:N]
str3 += ch

str4=str1+str3
print(str4)

'''
Program 20. Write a program which accepts two strings from the user
and compares only first N characters of two strings. If both strings are
equal till first N characters then return 0 otherwise return difference
between first mismatch character (Implement strncmp()).
Input: FirStr FirNew
N : 3
Output: Both strings are equal.
'''
str1 = input("Enter string : ")
str2 = input("Enter string : ")

str3=''
str4=''
N = int(input("Enter number :"))
ch=str2[0:N]
str3 += ch

ch1 = str1[0:N]
str4 += ch1

if str3==str4:
    print ("Both strings are equal ")
else:
    print("Differance between first mismatch character")


