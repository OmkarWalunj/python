'''
Program 1. Write a program which accepts a sentence from the
user and print that sentence.
Input: My name is
Output: My name is
'''
str1=input ("Enter sentence : ")
print(str1)
'''
Program 2. Write a program which accepts a string from the user
which contains characters from ‘b’ to ‘y’.
Input: mn jn kn kazfd
Output: mn jn kn k
'''
str2=input("Enter string : ")

for x in str2:
    if x >= 'b' and x<='y':
        print(x,end=' ')
print()

'''
Program 3. Write a program which accepts sentences from the user
and print a number of small letters, capital letters and digits from that
sentence.
Input: abcDE 5Glm1 O
Output: Small:5 Capital: 4 Digits: 2
'''

str3=input("Enter string : ")

small=0
capital=0
digit=0
for x in str3:
    if x >= 'a' and x<='z':
        small=small+1
    if x>= 'A' and x<= 'Z':
        capital = capital+1
    if x>='0' and x<='9':
        digit = digit+1
print("Small : " ,small,"capital : ",capital,"digit : ",digit)

'''
Program 4. Write a program which accepts sentences from the user
and print length of that sentence (Implement strlen()).
Input: India is my
Output: 11
'''
str4=input("Enter string :")
count=0
for x in range(len(str4)):
    count +=1
print(count)
