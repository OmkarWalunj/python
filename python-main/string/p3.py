
'''
Program 8. Write a program which accepts sentences from the user
and prints the last word from that sentence.
Input: In my company
Output: company
'''
str1 = input("Enter string : ")

str2= str1.split(' ')
print(" last word is :",str2[-1])

'''
Program 9. Write a program which accepts sentences from user and
position from user and print the word at that position.
Input: is my he she
Position: 3
Output: he
'''
str1 = input("Enter string : ")

str2= str1.split(' ')
position = int (input("Enter Position : "))
print(str2[position])

'''
Program 10. Write a program to convert the string from upper case to
lower case (Implement strlwr()).
Input: DevIce DriVer
Output: device driver
'''
str3 = input("Enter  string : ")

ch2=''
for x in range(len(str3)):
    ch=ord(str3[x])
    if ch>=65 and ch<=91:
        ch2 += chr(ch+32)
    else:
        ch2 += chr(ch)
print(ch2)

'''
Program 11. Write a program to convert the string from lowercase to
uppercase (Implement strupr()).
Input: DevIce DriVer
Output: DEVICE DRIVER
'''
str4 = input("Enter  string : ")

ch3=''
for x in range(len(str3)):
    ch=ord(str4[x])
    if ch>=97 and ch<=122:
        ch3 += chr(ch-32)
    else:
        ch3 += chr(ch)
print(ch3)

