'''
Program 29. Write a program which sets all characters in string
to a specific character (Implement strset()).
Input: HelloWorld
Char : a
Output: aaaaaaaaaa
'''
str1 = input("Enter string : ")

char=input(" Enter Char :")
str2 = ''
for x in str1:
    x=char
    str2 += x
print(str2)

'''
Program 30. Write a program which sets first N characters in string
to a specific character (Implement strnset()).
Input: HelloWorld a
N : 8
Output: aaaaaaaald
'''
str1 = input("Enter string : ")

N = int ( input("Enter number :"))
char=input(" Enter Char :")
str2 = ''
for x in str1[0:N+1:1]:
    x=char
    str2 += x 
print(str2+str1[N+1::1])

'''
Program 31. Write a program which sets last N characters in string
to a specific character
Input: HelloWorld a
N : 8
Output: Heaaaaaaaa
'''

str1 = input("Enter string : ")

N = int ( input("Enter number :"))
char=input(" Enter Char :")
str2 = ''
for x in str1[-1:-N-1:-1]:
    x=char
    str2 += x
print(str1[0:N+1:1]+str2)

'''
Program 32. Write a program which accepts string from the user and
searches for the first occurrence of a specific character in string and
returns the position at which character is found (Implement strchr()).
Input: India is my country.
Enter Char : m
Output: Character m is found at position 9
'''

str1 = input("Enter string : ")
char = input(" Enter char : ")

for x in range(len(str1)):
    if str1[x] == char :
        print("character ",char,"is found at position ",x)
'''
Program 33. Write a program which accepts string from user and
search last occurrence of specific character in string and return the
position at which character is found (Implement strchr()).
Input: India is my country.
Enter char : n
Output: Character n is found at position 15
'''
str1 = input("Enter string : ")
char = input(" Enter char : ")

i=1
while i<=len(str1):
    if str1[-i] == char :
        print("character ",char,"is found at position ",i)
    i= i+1


