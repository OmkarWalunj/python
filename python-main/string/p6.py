'''
Program 21. Write a program which accepts two strings from the user
and compare two strings without case sensitivity. If both strings are
equal then return 0 otherwise return difference between first mismatch
character (Implement stricmp()).
Input: FirStr FIRStROutput: Both strings are equal.
'''
str1 = input("Enter String : ")
str2 = input("Enter String : ")

ch3 = ''

for x in range (len(str2)):
    ch=ord(str2[x])
    if ch>=65 and ch<=90:
        ch3 += chr(ch+32)
    else:
        ch3 += chr(ch)
ch4 = ''

for x in range (len(str1)):
    ch=ord(str1[x])
    if ch>=65 and ch<=90:
        ch4 += chr(ch+32)
    else:
        ch4 += chr(ch)
if ch3==ch4:
    print("Both string are equal")
else:
    print("Differance between first mismatch character ")
'''
Program 22. Write a program which accepts string from the user and
then reverse the string without taking another string (Implement
strrev()).
Input: Hello World
Output: dlroW olleH
'''
str1 = input("Enter String : ")

i=1
while i <= len (str1):
    print(str1[-i],end=' ')
    i += 1
print()

# OR
str1=input("Enter string :")
print(str1[-1::-1])
'''
Program 23. Write a program which accepts string from the user and
then reverse the string till first N characters without taking another
string.
Input: Hello World
N : 5
Output: olleH World
'''
str1 = input("Enter String : ")

N=int (input("Enter number: "))

print(str1[N::-1]+str1[N::1])
'''
Program 24. Write a program which accepts string from the user and
then reverse the string till the last N characters without taking
another string.
Input: Hello World
N : 5
Output: Hello dlroW
'''
str1 = input("Enter String : ")

N=int (input("Enter number: "))

print(str1[0:len(str1)-N:1]+str1[-1:-N-1:-1])
'''
Program 25. Write a program which accepts strings from the user and
then accepts a range and reverse the string in that range without
taking another string.
Input: Hello World
Start: 3
End : 8
Output: HeoW ollrld
'''
str1 = input ("Enter String : ")
start = int(input("Enter start range : "))
end= int(input("Enter end range : "))

print(str1[0:start:]+str1[end:start-1:-1]+str1[end+1::1])
'''
Program 26. Write a program which accepts strings from the user and
reverse words from that string.
Input: Hello World
Output: olleH dlroW
'''
str1 = input("Enter string : ")

str2=str1.split(' ')
str3=''
for x in str2:
    x=x[-1::-1]
    str3 +=x+' '
print(str3)

'''
Program 27. Write a program which accepts strings from the user and
reverse words from that string which are of even length.
Input: New HO abcd can
Output: New OH dcba can
'''
str1 = input("Enter string : ")

str2=str1.split(' ')
str3=''
for x in str2:
    if len(x)%2==0:
        x=x[-1::-1]
        str3 +=x+' '
    else:
        str3 += x+' '
print(str3)

'''
Program 28. Write a program which accepts strings from the user
and check whether the string is palindrome or not.
Input: level
Output: String is palindrome.
'''
str1=input("Enter string :")
a=-1
flag = 0
for x in range(len(str1)):
    if str1[0] == str1[-1]:
        flag +=1
        print("string is palindrome")
        break
if (flag == 0):
    print("String is not palindrome")


