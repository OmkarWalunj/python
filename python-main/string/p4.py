'''
Program 12. Write a program which toggles the case of a string.
Input: DevIce DriVer
Output: dEViCE dRIvER
'''

str1 = input("Enter string :")
ch2=''
for x in range(len(str1)):
    ch=ord(str1[x])
    if ch>=65 and ch<=90:
        ch2 += chr(ch+32)
    elif ch>=97 and ch<=122:
        ch2 += chr(ch-32)
    else:
        ch2 += chr(ch)
print(ch2)

'''
Program 13. Write a program to check whether given strings are
Anagram strings or not.
Input: abccd cbcda
Output: Strings are anagram
Input: shashi ashish
Output: Strings are anagram
'''
str2= input("Enter string :")
ch3 = ''

for x in range (len(str2)):
    ch=ord(str2[x])
    if ch>=65 and ch<=90:
        ch3 += chr(ch+32)
    else:
        ch3 += chr(ch)
str3=str2.split(' ')

a= len(str3[0])
b= len(str3[1])
if a==b:
    print("Strings are anagram")
else:
    print("String are not anagram")


'''
Program 14. Write a program which accepts a string from the user and
copy that string into some other string (Implement strcpy()).
'''

str4= input("Enter string: ")

ch=''
for x in range(len(str4)):
    ch1=str4[x]
    ch += ch1
print(ch)
'''

Program 15. Write a program which accepts strings from the
user and copy first N characters into some destination string
(Implement strncpy()).
Input: India is my Country
N : 8
Output: India is
'''
str7=input("Enter String : ")

str8=''
N = int(input("Enter number :"))
ch=str7[0:N]
str8 += ch
print(str8)
'''
Program 16. Write a program which accepts strings from the user and
accept number N then copy the last N character into some other
string.
Input: India is my
N : 5
Output: is my
'''
str5 = input("Enter string : ")

str6=''
N = int(input("Enter number :"))
a= len(str5)-N
ch=str5[a:]
str6 += ch
print(str6)


