''' Nested loop   '''
'''write a program 4 * 4 matrix
   1 2 3 4 
   A B C D 
   1 2 3 4 
   A B C D
'''

rows = int(input ("Enter no of rows :"))
cols = int(input ("Enter no of cols :"))

for i in range(rows):
    num = 65
    for j in range(cols):
        if i % 2==0:
            print (num-64,end=" ")
            num = num+1
        else:
            print(chr(num),end=" ")
            num+=1
    print()
    
