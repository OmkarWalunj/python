''' Nested loop   '''
'''write a program 4 * 4 matrix of "*" and"#"
     * * * 
     # # # 
     * * *
'''

rows = int(input ("Enter no of rows :"))
cols = int(input ("Enter no of cols :"))

for i in range(rows):
    for j in range(cols):
        if i % 2==0:
            print ("*",end=" ")
        else:
            print("#",end=" ")
    print()
    
