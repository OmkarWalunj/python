
'''
Wap to print 3D array in numpy
'''

import numpy

arr = numpy.array([[[1,2,3],[4,5,6],[7,8,9]],[[10,11,12],[13,14,15],[16,17,18]]],int)

print(arr,end=' ')
print()

for x in range(len(arr)):
    for y in range(len(arr[x])):
        for z in range(len(arr[x][y])):
            print(arr[x][y][z],end=' ')
        print()
    print()

