try:
    print("in outer try")
    
    try:
        print("IN inner try")
        x=10/0
    except ValueError:
        print("in inner except")
    finally:
        print("in Outer finally")
except ZeroDivisionError:
    print("in outer except")
finally:
    print("in outer except")
