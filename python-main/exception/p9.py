class NoAccessToLeetCode(Exception):
    def __init__(self,msg):
        self.msg = msg
class Leetcode:
    def content(self):
        print("Leetcode content : access")
class Bootcamp:

    def __init__(self, name,numProSolve):
        self.name = name
        self.numProSolve=numProSolve;

    def checkCode(self,course):
        
        if self.numProSolve < 600:
            raise NoAccessToLeetCode("{},you are not eligible to access course".format(self.name))
        else:
            course.content()
ltCode = Leetcode()

btCamp=Bootcamp("Ajinkya ",550)
try:
    btCamp.checkCode(ltCode)
except NoAccessToLeetCode as msg:
    print(msg)


