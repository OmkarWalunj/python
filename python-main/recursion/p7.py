#   returning  function from a function

def fun(num):
    def prime(num):
        flag=0
        if num > 1:
            for i in range(2,int(num/2)+1):
                if num% i==0:
                    flag =1
                    break
            if flag==0:
                return num,"is Prime"
            else:
                return num,"is not prime"
        else:
            return num,"is not prime"
    def palindrome(num):
        temp=num
        rev=0
        while (num>0):
            dig=num%10
            rev=rev*10+dig
            num=num//10
        if (temp==rev):
            return temp,"is palindrome"
        else :
            return temp,"is not Palindrome"
    def amstrong(num):
        sum = 0
        temp = num
        while temp > 0:
            digit = temp % 10
            sum += digit ** 3
            temp //= 10
        if num == sum:
            return num,"is an Armstrong number"
        else:
            return num,"is not an Armstrong number"
    def reverse(num):
        str1=str(num)
        return str1[-1::-1]

    return prime,palindrome,amstrong,reverse
num=int(input("Enter Number :"))
while (True):
    print("\n 1.prime number,\n 2.palindrome number ,\n 3.amstrong number,\n 4.reverse number,\n 5.exit")
    retfun=fun(num)
    ch=int(input("Enter your choice :"))
    if ch==1:
        print(retfun[0](num))
    elif ch==2:
        print(retfun[1](num))
    elif ch==3:
        print(retfun[2](num))
    elif ch==4:
        print(retfun[3](num))
    elif ch==5:
        break
    elif ch==0:
        break



