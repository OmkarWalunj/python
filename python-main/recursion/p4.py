import numpy
def reverseArr(arr):
    if len(arr)==0:
        return 0
    return reverseArr(arr[-2::-1]),arr[-1]
num=int(input("Enter size of array ? "))
arr=numpy.zeros(num,int)

for x in range(num):
    arr[x]=int(input("Enter number:"))
print(reverseArr(arr))
