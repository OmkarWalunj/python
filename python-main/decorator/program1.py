def mult(fun):
    print("mult")
    def inner (x):
        print("inner")
        ans= fun(x)
        return ans*x
    return inner
@mult
def sqr (x):
    print("sqr")
    return x*x
print(sqr(5))

             # With out using decorator
def mult(fun):
    print("mult")
    def inner (x):
        print("inner")
        ans= fun(x)
        return ans*x
    return inner
def sqr (x):
    print("sqr")
    return x*x
retFun = mult(sqr)
print(retFun(5))

