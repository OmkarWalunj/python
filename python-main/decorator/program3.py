def reverse(fun):
    print("reverse string")
    def inner (*args):
        print("Inner 1")
        str1= fun(*args)
        return str1[-1::-1]
    return inner
def concat(fun):
    print("concate string")
    def inner (*args):
        print("Inner 2")
        retFun=fun(*args)
        str3=''
        for x in retFun:
            str3 += x
        return str3
    return inner
@concat
@reverse
def printstring (str1,str2):
    print("main string")
    return str1,str2
print(printstring("Omkar","Walunj"))
