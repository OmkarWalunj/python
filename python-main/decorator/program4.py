def palindrome(fun):
    def inner (num):
        ans=fun(num)
        if ans == num:
            return num ,"is palindrome Number"
        else:
            return num,"is not palindrome Number"
    return inner
def reverse(fun):
    def inner (num):
        temp=fun(num)
        sum=0
        while (temp!=0):
            reminder = temp%10
            sum= (sum*10)+reminder
            temp=temp//10
        return sum
    return inner
@palindrome
@reverse
def printstring (num):
    return num
print(printstring(int(input("Enter Number: "))))
