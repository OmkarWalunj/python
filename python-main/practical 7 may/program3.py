''' WAP to take numerical input from the user
  and find whether the number is divisible by 5 and 11.
  input=55
  output = yes

  input = 15
  output = no
  '''
a=int (input("Enter Number:"))

if a % 5 ==0 and a % 11 ==0:
    print("yes")
else:
    print("no")
