'''
  WAP to Create array get size of array and values from user and
  Print of sum of that numbers
'''
import array

arr = array.array('i',[])

x = int (input("Enter Size of Array: "))

sum=0
for ele in range (x):
    num= int(input("Enter Number: "))
    arr.append(num)
    sum= sum+num
print(arr)                                     #array('i', [23, 32, 12, 23, 25])
print("Sum of number in array is : ",sum)      #Sum of number in array is :  115

