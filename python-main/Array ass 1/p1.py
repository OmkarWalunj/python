'''
Wap to print array '''

#####Create Array #####

import array
arr1= array.array('u',['A','B','C','D'])
arr2 = array.array('i',[10,20,30,40,50])
arr3=array.array('f',[10.2,20.3,30.4])
arr4=array.array('u',[])

##########Acessing element in array #######
print(arr1)
print(arr2)
print(arr3)
print(arr4)

######### OR  #########
for ele in arr1:
    print(ele)
for ele in arr2:
    print(ele)
for ele in arr3:
    print(ele)

########Assigning Array to another Array (copy) #######

arr5=arr1

print(arr5)
              #arr5 id = arr1 id is same

print(id(arr1))               #2167290297568

print(id(arr5))               #2167290297568


