'''
WAP to print sign and unsign using code
'''

import array

arr=array.array('i',[10,20,30,40,5])

print(arr)

arr1=array.array('I',[10,20,30,40,5])
print(arr1)

######## error of unsign#####

arr2=array.array('I',[10,20,-30,40,5])
print(arr2)        #OverflowError: can't convert negative value to unsigned int
