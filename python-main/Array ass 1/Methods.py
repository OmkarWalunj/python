'''
WAP to print Methods using code
append()
count()
index()
insert()
pop()
remove()
reverse()
tolist()
fromlist()
itemsize
buffer_info
'''
import array

arr=array.array('i',[10,20,30,40,5])

print(arr)

#arr[5]=50
#print(arr)                                                       #IndexError: array assignment index out of range

##########          Append()    ###########
#Error 1:
#array.append
#print(arr)                                                       #AttributeError: module 'array' has no attribute 'append'

#Error 2
#array.array.append(50)                                           #TypeError: descriptor 'append' for 'array.array' objects doesn't apply to a 'int' object
#print(arr)

arr.append(50)                                                    #array('i', [10, 20, 30, 40, 5, 50])
print(arr)

arr.append(60)                                                    #array('i', [10, 20, 30, 40, 5, 50,60])
print(arr)
 
arr.append(30)                                                    #array('i', [10, 20, 30, 40, 5, 50,30])
print(arr)

##############     Count()      ############
print(arr.count(30))                                              #30 is two times then o/p is 2
print(arr.count(10))                                              #10 is one times then o/p is 1

#############       index()     ############
print(arr.index(30))                                              #2
print(arr.index(10))                                              #0
#print(arr.index(70))                                             #ValueError: array.index(x): x not in array

###########        insert()     #############
arr.insert(3,35)
print(arr)

###########        pop()        #############
arr.pop(5)                                                        #array('i', [10, 20, 30, 35, 40, 50, 60, 30])
print(arr)

###########        remove()     #############
arr.remove(50)                                                    #array('i', [10, 20, 30, 35, 40, 60, 30])
print(arr)

###########        reverse()    #############
arr.reverse()                                                     #array('i', [30, 60, 40, 35, 30, 20, 10])
print(arr)

###########        tolist()     #############
lst=arr.tolist()                                                  #[30, 60, 40, 35, 30, 20, 10]
print(lst)

###########        fromlist()   ############# 
arr.fromlist(lst)                                                 #array('i', [30, 60, 40, 35, 30, 20, 10, 30, 60, 40, 35, 30, 20, 10])
print(arr)

###########        itemsize()   #############                                                  
print(arr.itemsize)                                               #4

###########        buffer_info  #############                                                  
print(arr.buffer_info)                                            #<built-in method buffer_info of array.array object at 0x000001A1390E14E0><built-in method buffer_info of array.array object at 0x000001A1390E14E0>
