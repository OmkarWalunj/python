'''
  WAP to get values from user and Check array value is present or not if value is present then
  Print of index of that numbers
'''
import array

arr = array.array('i',[5,25,12,7,11])

num = int (input("Enter num: "))

flag=0
for ele in arr:
    if (num==ele):
        flag=1
        print(arr.index(ele))
if flag==0:                                     
    print("number not found")
    

                                       #### OR #####
if num in arr:
    print(arr.index(num))
else:
    print("Number not found")
