import numpy

arr1 = numpy.array([10,20,30,40,50])

#slicing in 1Darray
ans=arr1[0:]  #10,20,30,40,50
print (ans)
ans=arr1[0:4]   #10,20,30,40
print (ans)
ans=arr1[2:5]   #30,40,50
print (ans)
ans=arr1[-1:-4:-2] #50,30
print (ans)
ans=arr1[-1:-4:2]  #[]
print (ans)
print(arr1[1:5]) #20,30,40,50
print(arr1[1:10:2]) #20,40
print(arr1[-7:-1:-1])  #[]
print(arr1[0:5])  #10,20,30,40,50
print(arr1[0:5:2]) #10,30,50
print(arr1[0:6:2]) #10,30,50
print(arr1[6:2:-1]) #50,40
print(arr1[-3:-5:-1]) #30,20
print(arr1[-6:-1])  #10,20,30,40
print(arr1[-3:-1:-1])#[]
print(arr1[2:-1])#30,40
print(arr1[4:-1])#[]
print(arr1[0:-1:2])#10,30
print(arr1[-4:4])#20,30,40
print(arr1[-4:2:-1])#[]

# slicing in 2Darray

arr2= numpy.array([[1,2,3],[4,5,6],[7,8,9]],int)

print(arr2[0])  #1,2,3
print(arr2[0][2])#3
print(arr2[0,2])#3
print(arr2[0:2:1])#[1,2,3],[4,5,6]
print(arr2[2::])#[7,8,9]
print(arr2[1:2:1])#[4,5,6]
print(arr2[-1:-3:])#[]
print(arr2[-3:-1:])#[1,2,3],[4,5,6]
print(arr2[2,1]) #[8]
print(arr2[-3:-1:-2,1])#[]
print(arr2[-1,1:-1])#8




