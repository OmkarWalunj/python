'''
WAP to find the third largest element from an array
Ip: [ [1,2,4]
[5,3,9]
[8,6,11]]
Op: third largest element is 8
'''
import numpy

rows= int (input("Enter no of rows in array: "))
cols= int (input("Enter no of cols in array: "))
arr1 = numpy.zeros((rows,cols),int)

for x in range(rows):
    for y in range(cols):
        val=int(input("Enter no :"))
        arr1[x][y]=val
print(arr1)

arr2 = arr1.flatten()
arr3 = numpy.sort(arr2)
print("third largest element is: ",arr3[-3])


