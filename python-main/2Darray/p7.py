'''

Write a java program to take a 2-D array of order 3 X 3 and Sort
that array in ascending order and print it as before and after operation
'''
import numpy

rows= int (input("Enter no of rows in array: "))
cols= int (input("Enter no of cols in array: "))
arr1 = numpy.zeros((rows,cols),int)

for x in range(rows):
    for y in range(cols):
        val=int(input("Enter no :"))
        arr1[x][y]=val
print(arr1)
arr2=arr1.flatten()
arr3=numpy.sort(arr2)
arr4=numpy.reshape(arr3,(rows,cols))
print(arr4)
