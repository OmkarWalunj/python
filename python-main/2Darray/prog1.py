
#     Two dimensional array
import numpy 

arr1 =numpy.array([10,20,30,40,50],int)
arr2 = numpy.array([[10,20,30,40,50],[1,2,3,4,5]],int)

print(arr1)
print(arr2)              #[[10 20 30 40 50]
                        # [ 1  2  3  4  5]]
########### Acessing array ele in two dimentional array ###########

print(arr2[0][2])                      #30
print(arr2[0][4])                      #50
print(arr2[1][4])                      #5
print(arr2[1][3])                      #4
#print(arr2[2][4])              #IndexError: index 2 is out of bounds for axis 0 with size 2

############ Atrributes of numpy.array 
           # 1) ndim
print(arr1.ndim)             # 1
print(arr2.ndim)             # 2
           # 2) size
print(arr1.size)             # 5
print(arr2.size)             # 10
           # 3) itemsize
print(arr1.itemsize)         #  8
print(arr2.itemsize)         #  8
           # 4) nbytes
print(arr1.nbytes)           #  40
print(arr2.nbytes)           #  80
           # 5) shape
print(arr1.shape)            # (5,)
print(arr2.shape)            # (2,5)
           # 6) dtype
print(arr1.dtype)            # int64
print(arr2.dtype)            # int64
