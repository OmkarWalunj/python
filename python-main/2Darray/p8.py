'''
Write a program to create a 2d array of integer elements.
Take the number of rows and columns values from the user.
And print a 2d array of numbers whose first digit is N,
take the N value from the user.
Input:
Enter number of Rows = 2
Enter number of Column = 2
Enter value of N = 3
Output:
3 30
31 32
'''
import numpy

rows= int (input("Enter no of rows in array: "))
cols= int (input("Enter no of cols in array: "))
n= int(input("Enter value of n is : "))
arr1 = numpy.zeros((rows,cols),int)
for x in range(rows):
    for y in range(cols):
        arr1[0][0]=n
        val=int(input("Enter no :"))
        arr1[x][y]=val
print(arr1)

