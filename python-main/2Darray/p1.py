
'''WAP to convert a 1D array into 2D array '''

import numpy

arr1 = numpy.array([10,20,30,40,50,60,70,80,90],int)

print(arr1)

arr2 = arr1.reshape(3,3)

print(arr2)
