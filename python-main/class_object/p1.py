
class Cric:
    matchformat='T20'
    def __init__(self):
        self.name="Dhoni"
        self.jerno=7
    def display(self):
        print(self.name)
        print(self.jerno)
    def myclassmethod(fun):
        def inner(*args):
            fun(args[0].__class__)
        return inner
    @myclassmethod
    def displayformat(cls):
        print(cls)
        print(cls.matchformat)
obj1 = Cric()
obj1.display()
print(obj1)
obj1.displayformat()

