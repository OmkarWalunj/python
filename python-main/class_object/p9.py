
class Parent:
    x=10
    def __init__(self):
        print("Parent Constructor")
        self.y=20
    def display(self):
        print(self.x)
        print(self.y)
class Child(Parent):
    x=110
    def __init__(self):
        super().__init__()
        print("CHild Constructor")
        self.y=220
    def ChildData(self):
        print(super().x)
        print(self.x)
   #     print(Parent.x)
        print(self.y)

obj=Child()
obj.ChildData()
obj.display()


