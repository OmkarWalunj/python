class Outer:
    def __init__(self):
        print("Outer Constructor")
        self.x=10
    def display(self):
        print(self.x)
        class Inner :
            def __init__(self):
                print("Inner Constructor")
                self.y=20
            def dispData(self):
                print(self.y)
        innobj=Inner()
        innobj.dispData()
        print(innobj.__class__)
outObj=Outer()
outObj.display()
